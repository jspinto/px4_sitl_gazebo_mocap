
#include "ros/ros.h"
#include "gazebo_msgs/ModelStates.h"
#include "geometry_msgs/Quaternion.h"
#include "geometry_msgs/PoseStamped.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"

#include <random>
#include <string>
#include <thread>
#include <atomic>
#include <vector>
#include <signal.h>

#define PI 3.14159265359

using namespace ros;

std::vector<geometry_msgs::Pose> ros_pose;

std::default_random_engine generator;
std::normal_distribution<float> dist_pose(0.0,0.004);
std::normal_distribution<float> dist_att(0.0,0.4*PI/180);

// Handle Ctrl+C 
std::atomic<bool> sigint;

void add_noise(geometry_msgs::Pose& pose){

    // Add noise to the pose retrieved from Gazebo
    double roll, pitch, yaw;

    pose.position.x += dist_pose(generator);
    pose.position.y += dist_pose(generator);
    pose.position.z += dist_pose(generator);

    tf2::Quaternion q(pose.orientation.x, pose.orientation.y, 
                                        pose.orientation.z, pose.orientation.w);

    tf2::Matrix3x3 m(q);
    m.getRPY(roll,pitch,yaw);

    roll += dist_att(generator);
    pitch += dist_att(generator);
    yaw += dist_att(generator);

    q.setRPY(roll,pitch,yaw);
    pose.orientation = tf2::toMsg(q);

}

void pose_publisher(int id, const char* n_space){

    NodeHandle n;

    std::string ns = "/" + std::string(n_space);
    Publisher pose_pub = n.advertise<geometry_msgs::PoseStamped>(ns+"/mavros/vision_pose/pose", 1);

    Rate rate(60.0);

    geometry_msgs::PoseStamped p;
    
    while (ok()){

        if (sigint) break;

        p.header.stamp = Time::now();

        add_noise(ros_pose[id]);
        p.pose = ros_pose[id];

        pose_pub.publish(p);
        rate.sleep();
    }
    
}

void update_pose(const gazebo_msgs::ModelStates::ConstPtr& msg){

    // pose[0] corresponds to the ground plane
    ros_pose = msg->pose;

}

void pose_subscriber(){

    NodeHandle n;

    Subscriber pose_sub = n.subscribe<gazebo_msgs::ModelStates>("gazebo/model_states", 10, update_pose);

    Rate rate(60.0);

    while (ok()){

        if (sigint) break;

        spinOnce();
        rate.sleep();
    }

}

void signal_cb(int signum){

    sigint = true;
}

int main(int argc, char **argv){

    init(argc, argv, "mocap_emulator", init_options::NoSigintHandler);
    NodeHandle n;

    // Custom SIGINT (ensure that threads are joined)
    signal(SIGINT, signal_cb);
    sigint = false;

    ROS_INFO("MCS: Starting...");
    
    std::vector<std::thread> threads;

    threads.reserve(3);
    threads.emplace_back( std::thread(pose_subscriber) );

    // Wait long enough for the callback...
    Rate rate(0.5); 
    rate.sleep();

    for(int j = 1; j < argc; j++)
        threads.emplace_back( std::thread(pose_publisher,j,argv[j]) );
    

    for(auto& th : threads)
        th.join();

    return 0;
}