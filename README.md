# PX4_SITL_Gazebo_Mocap #

The original PX4 SITL setup in Gazebo relies on a GPS plug-in to estimate the state of the simulated 
drones, however, to create a simulation environment which resembles closer an indoor flying arena 
featuring a motion capture system (mocap), one should generate synthetic mocap data and fuse it with 
the readings of other simulated sensors on board the vehicles.

In this package, the synthetic visual information is obtained by adding white Gaussian noise to the
pose of the vehicles in the simulation and then it is forwarded to the autopilot software. By setting 
some of the parameters of the PX4 autopilot, one can enable the fusion of 'vision' data.

### PX4 State Estimator Parameters Configuration ###

Assuming you have cloned the [PX4](https://github.com/PX4/PX4-Autopilot) autopilot repository already,
find and edit the file **/Firmware/ROMFS/px4fmu_common/init.d-posix/rcS** which stores the autopilot internal 
configurations. Follow the instructions available on [this](https://docs.px4.io/master/en/ros/external_position_estimation.html) page of the PX4 user guide to set the EKF parameters which enable the fusion of vision data.

### Running the program ###

Firstly launch the PX4 SITL Gazebo, then run the program using the command:
```
rosrun px4_sitl_gazebo_mocap mocap_node <ns_1> <ns_2> ... <ns_N>
```
where 'ns_#' is the namespace associated with vehicle #. Open the launch file
you just used to run the simulation to find the namespaces of the vehicles involved 
in the simulation; 'N' is the total number of vehicles.

You may alternatively add a new line in your launch file with the following:
```
<node pkg="px4_sitl_gazebo_mocap" name="mcs" type="mocap_node" args="$(arg ns_1) $(arg ns_2) ... $(arg ns_N)" output="screen"/>
```
Note you will need to define the args 'ns_1', 'ns_2', ..., 'ns_N' _a priori_.

The output of the program can be seen in the figure below.
If the synthetic visual data is sucessfully fused, the autopilot will let you know.

![figure](https://i.imgur.com/NKMuL51.png)

### Acknowledgements ###

The author would like to acknowledge the contribuition of Tiago Oliviera to the development of this ROS package.


